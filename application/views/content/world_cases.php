<div class="container my-5 pt-5">
    <div class="col-12 mt-5 text-center">
        <h2>World Covid19 Data Cases</h2>
    </div>
    <div class="row">
        <div class="col-lg-4 col-md-4 col-sm-12">
            <div class="card-body bg-danger my-5 text-white">
                <div class="row">
                    <div class="col-4 p-3">
                        <i class="fa fa-plus fa-3x"></i>
                    </div>
                    <div class="col-8">
                        <p>Confirmed Cases</p>
                        <h4 class="confirmed">loading..</h4>
                    </div>
                    <p class="card-text"><small class="waktu" style="text-align: right;"></small></p>
                </div>
            </div>
        </div>

        <div class="col-lg-4 col-md-4 col-sm-12">
            <div class="card-body bg-success my-5 text-white">
                <div class="row">
                    <div class="col-4 p-3">
                        <i class="fa fa-check fa-3x"></i>
                    </div>
                    <div class="col-8">
                        <p>Recovered Cases</p>
                        <h4 class="recovered">loading..</h4>
                    </div>
                    <p class="card-text"><small class="waktu" style="text-align: right;"></small></p>
                </div>
            </div>
        </div>

        <div class="col-lg-4 col-md-4 col-sm-12">
            <div class="card-body bg-danger my-5 text-white">
                <div class="row">
                    <div class="col-4 p-3">
                        <i class="fa fa-skull-crossbones fa-3x"></i>
                    </div>
                    <div class="col-8">
                        <p>Death Cases</p>
                        <h4 class="death">loading..</h4>
                    </div>
                    <p class="card-text"><small class="waktu" style="text-align: right;"></small></p>
                </div>
            </div>
        </div>

    </div>

    <div class="card my-5">
        <div class="card-body">
            <h4 class="card-title">Indonesia</h4>
            <canvas id="chart"></canvas>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        $.ajax({
            url: "https://api.covid19api.com/summary",
            type: "GET",
            dataType: "JSON",
            success: function(data) {
                // console.log(data.Global.TotalConfirmed)
                $(".confirmed").html(data.Global.TotalConfirmed)
                $(".recovered").html(data.Global.NewRecovered)
                $(".death").html(data.Global.NewDeaths)
                $(".waktu").html(formatDate(data.Global.Date))
            },
            error: function(jqXHR, textStatus, errorThrown) {
                $(".confirmed").html("error")
                $(".recovered").html("error")
                $(".death").html("error")
            }
        });
    })
    $.ajax({
        url: "https://api.covid19api.com/country/indonesia/status/confirmed?from=2021-09-01T00:00:00Z&to=2021-10-01T00:00:00Z",
        method: "GET",
        success: function(data) {
            console.log(data);
            var label = [];
            var value = [];
            for (var i in data) {
                label.push(formatDateMonth(data[i].Date));
                value.push(data[i].Cases);
            }
            var ctx = document.getElementById('chart').getContext('2d');
            var chart = new Chart(ctx, {
                type: 'bar',
                data: {
                    labels: label,
                    datasets: [{
                        label: 'Confirmed cases 2021-09-01 - 2021-10-01',
                        backgroundColor: 'rgb(252, 116, 101)',
                        borderColor: 'rgb(255, 255, 255)',
                        data: value
                    }]
                },
                options: {}
            });
        }
    });

    function formatDate(date) {
        var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;

        return [year, month, day].join('-');
    }

    function formatDateMonth(date) {
        var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;

        return [month, day].join('-');
    }
</script>